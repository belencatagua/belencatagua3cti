﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ModeloEmpresa
{
    class EmpleadoFijo 
    {
        
        private int añoentrada;
        
        public int AñoEntrada
        {
            get { return añoentrada; }
            set { añoentrada = value; }
        }

        private int sueldobase;

        public int SueldoBase
        {
            get { return sueldobase; }
            set { sueldobase = value; }
        }

        private int compleanual;

        public int CompleAnual
        {
            get { return compleanual; }
            set { compleanual = value; }
        }

        public Double CompleFijo
        {
            get { return CompleAnual * AñoEntrada; }
        }

        public Double SueldoPagar
        {
            get { return SueldoBase + CompleFijo; }
        }
    }
}
