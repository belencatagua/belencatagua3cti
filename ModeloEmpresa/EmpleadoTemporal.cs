﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ModeloEmpresa
{
    class EmpleadoTemporal 
    {
        private String fechaingreso;
        private String fechasalida;

        public String FechaIngreso
        {
            get { return fechaingreso; }
            set { fechaingreso = value; }
        }

        public String FechaSalida
        {
            get { return fechasalida; }
            set { fechasalida = value; }
        }

        private int sueldofijo;

        public int SueldoFijo
        {
            get { return sueldofijo; }
            set { sueldofijo = value; }
        }

    }
}
