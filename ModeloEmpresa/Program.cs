﻿using System;

namespace ModeloEmpresa
{
    class Program
    {
        static void Main(string[] args)
        {
            //EMPLEADO FIJO
            Empleado empleado = new Empleado();
            EmpleadoFijo empleadofijo = new EmpleadoFijo();
            empleado.Nombre  = "Belén";
            empleado.Edad = (20);
            empleado.Departamento = ("Administracion");
            empleadofijo.AñoEntrada = (5);
            empleadofijo.CompleAnual = (100);
            empleadofijo.SueldoBase = (294);
            Console.WriteLine("Nombre del empleado =" + empleado.Nombre + "\n" + "Edad del Empleado=" + empleado.Edad + "\n" +
                "Departamento del Empleado =" + empleado.Departamento + "\n" + "Año Entrada =" + empleadofijo.AñoEntrada + "\n" +
                "Complemento Anual =" + empleadofijo.CompleAnual + "\n" + "Sueldo Base =" + empleadofijo.SueldoBase +"\n");
            Console.WriteLine("********** SUELDO A PAGAR ***********");
            Console.WriteLine("     TOTAL A PAGAR: " + empleadofijo.SueldoPagar );
            Console.WriteLine("\n");

            // EMPLEADO TEMPORAL
            EmpleadoTemporal empleadotemporal = new EmpleadoTemporal();
            empleado.Nombre = "MASSIEL";
            empleado.Edad = (23);
            empleado.Departamento = ("Contadora");
            empleadotemporal.FechaIngreso = "19/ 04/2017";
            empleadotemporal.FechaSalida = "19/04/2019";
            empleadotemporal.SueldoFijo = (500);
            Console.WriteLine("Nombre del empleado =" + empleado.Nombre + "\n" + "Edad del Empleado=" + empleado.Edad + "\n" +
                "Departamento del Empleado =" + empleado.Departamento + "\n" + "Fecha Ingreso =" + empleadotemporal.FechaIngreso + "\n"
                + "Fecha Salida =" + empleadotemporal.FechaSalida + "\n" + "Sueldo Fijo =" + empleadotemporal.SueldoFijo + "\n");
            Console.WriteLine("********** SUELDO FIJO A PAGAR ***********");
            Console.WriteLine("     TOTAL A PAGAR: " + empleadotemporal.SueldoFijo);
            Console.WriteLine("\n");

            //EMPLEADO POR HORA
            EmpleadoHoras empleadoHoras = new EmpleadoHoras();
            empleado.Nombre = "EDGARDO";
            empleado.Edad = (25);
            empleado.Departamento = "Desarrollador";
            empleadoHoras.PrecioHora = (25);
            empleadoHoras.NumeroHoras = (5);
            Console.WriteLine("Nombre del empleado =" + empleado.Nombre + "\n" + "Edad del Empleado=" + empleado.Edad + "\n" +
                "Departamento del Empleado =" + empleado.Departamento + "\n" + "Precio por Horas =" + empleadoHoras.PrecioHora + "\n"
                + "Numeros de Horas =" + empleadoHoras.NumeroHoras + "\n");
            Console.WriteLine("********** SUELDO A PAGAR POR HORA ***********");
            Console.WriteLine("     TOTAL A PAGAR: " + empleadoHoras.SubTotal);
            Console.WriteLine("\n");
            
        }
    }
}
