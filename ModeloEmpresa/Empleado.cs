﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ModeloEmpresa
{
    public class Empleado
    {
        private String nombre;
        private int edad;
        private String departamento;
        public String Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }

        public int Edad
        {
            get { return edad; }
            set { edad = value; }
        }

        public String Departamento
        {
            get { return departamento; }
            set { departamento = value; }
        }

    }
}
