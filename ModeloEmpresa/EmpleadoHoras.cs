﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ModeloEmpresa
{
    public class EmpleadoHoras
    {
        private Double preciohora;
        private int numerohoras;

        public Double PrecioHora
        {
            get { return preciohora; }
            set { preciohora = value; }
        }

        public int NumeroHoras
        {
            get { return numerohoras; }
            set { numerohoras = value; }
        }

        public double SubTotal
        {
            get { return PrecioHora * NumeroHoras; }
        }
    }
}
